package IPL_PROJECT.Solution;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.lang.*;

public class TopEconomyBowler {
    public static void main(String[] args) {

        String matches = "/home/ubuntu/Desktop/Project/src/DataFile/matches.csv";
        String deliveries = "/home/ubuntu/Desktop/Project/src/DataFile/deliveries.csv";

        String lineM = "";
        String lineD = "";
        HashMap<String, Float> economyBowler = new HashMap<>();
        HashMap<String, Integer> legalDelivery = new HashMap<>();
        HashMap<String, Integer> totaRun = new HashMap<>();
        List<Map.Entry<String, Float>> sortEconomyBowler;

        try {
            BufferedReader bm = new BufferedReader(new FileReader(matches));

            while((lineM= bm.readLine())!=null) {
                String match[] = lineM.split(",");

                if(match[1].equals("2015")) {

                    BufferedReader bd = new BufferedReader(new FileReader(deliveries));
                    while ((lineD=bd.readLine())!=null)
                    {
                        String delivery[] = lineD.split(",");
                        if(match[0].equals(delivery[0]))
                        {
                            if(legalDelivery.containsKey(delivery[8]) && totaRun.containsKey(delivery[8]))
                            {
                                if(Integer.parseInt(delivery[10]) == 0 && Integer.parseInt(delivery[13]) == 0)
                                {
                                    legalDelivery.put(delivery[8],legalDelivery.get(delivery[8])+1);
                                }
                                totaRun.put(delivery[8],totaRun.get(delivery[8])+Integer.parseInt(delivery[17]));
                            }
                            else
                            {
                                legalDelivery.put(delivery[8],1);
                                totaRun.put(delivery[8],Integer.parseInt(delivery[17]));
                            }
                            economyBowler.put(delivery[8],totaRun.get(delivery[8])/(legalDelivery.get(delivery[8])/6f));
                        }
                    }
                }
            }
            System.out.println("Top 5 Economy Bowlers in year 2015 : ");

            sortEconomyBowler = new ArrayList<>(economyBowler.entrySet());
            sortEconomyBowler.sort(Comparator.comparing(Map.Entry::getValue));

            for(int i=1; i<=5; i++)
            {
                System.out.println(i+". "+sortEconomyBowler.get(i));
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    }

