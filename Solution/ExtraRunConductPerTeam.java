package IPL_PROJECT.Solution;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;
import java.util.HashMap;
import java.util.Map;

public class ExtraRunConductPerTeam {
    public static void main(String[] args) {

        String matches = "/home/ubuntu/Desktop/Project/src/DataFile/matches.csv";
        String deliveries = "/home/ubuntu/Desktop/Project/src/DataFile/deliveries.csv";

        String lineM = "";
        String lineD = "";

        HashMap<String, Integer> extraRun = new HashMap<>();

        try {
            BufferedReader bm = new BufferedReader(new FileReader(matches));

            while((lineM= bm.readLine())!=null) {
                String match[] = lineM.split(",");

                if(match[1].equals("2016")) {

                    BufferedReader bd = new BufferedReader(new FileReader(deliveries));
                    while ((lineD=bd.readLine())!=null)
                    {
                        String delivery[] = lineD.split(",");

                        if(match[0].equals(delivery[0])) {
                            if(extraRun.containsKey(delivery[3])) {
                                extraRun.put(delivery[3],extraRun.getOrDefault(delivery[3],0)+Integer.parseInt(delivery[16]));
                            }
                            else {
                                extraRun.put(delivery[3],1);
                            }
                        }

                    }

                }
            }
            System.out.println("Extra Run Conduct per team in 2016: ");

            for(Map.Entry<String,Integer> output : extraRun.entrySet()) {
                System.out.println(output.getKey()+" = "+ output.getValue());
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
