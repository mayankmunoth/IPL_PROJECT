package IPL_PROJECT.Solution;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MatchesWonByEveryTeam {
    public static void main(String[] args) {

        String matches= "/home/ubuntu/Desktop/IPL_Project/IPL_PROJECT/CSV_FILE/matches.csv";
        String line="";
        HashMap<String,Integer> wonByEveryteam=new HashMap<>();
        try {
            BufferedReader br=new BufferedReader(new FileReader(matches));
            while ((line=br.readLine())!=null)
            {
                String[] won=line.split(",");
                if(wonByEveryteam.containsKey(won[10]))
                {
                    wonByEveryteam.put(won[10],wonByEveryteam.get(won[10])+1);
                }
                else
                {
                    wonByEveryteam.put(won[10],1);
                }
            }
            System.out.println("Matches Won Every Team: ");

            for(Map.Entry<String,Integer> output : wonByEveryteam.entrySet())
            {
                System.out.println(output.getKey()+" = "+output.getValue());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
