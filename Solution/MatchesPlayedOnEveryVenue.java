package IPL_PROJECT.Solution;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class MatchesPlayedOnEveryVenue { public static void main(String[] args) {
    String matches= "/home/ubuntu/Desktop/IPL_Project/IPL_PROJECT/CSV_FILE/matches.csv";
    String line="";

    TreeMap<String,Integer> matchPlayedOnVenue=new TreeMap<>();

    try {
        BufferedReader br =new BufferedReader(new FileReader(matches));
        while ((line= br.readLine())!=null)
        {
            String[] venue = line.split(",");
            if(matchPlayedOnVenue.containsKey(venue[14]))
            {
                matchPlayedOnVenue.put(venue[14],matchPlayedOnVenue.get(venue[14])+1);
            }
            else
            {
                matchPlayedOnVenue.put(venue[14],1);
            }
        }

        System.out.println("Total Matches Played On Every Venue: ");

        for(Map.Entry<String,Integer> output : matchPlayedOnVenue.entrySet())
        {
            System.out.println(output.getKey()+" = "+output.getValue());
        }

    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
}
}


