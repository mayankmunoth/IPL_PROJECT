package  IPL_PROJECT.Solution;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class MatchesPlayedPerYear {

    public static void main(String[] args) {
        String matches= "/home/ubuntu/Desktop/IPL_Project/IPL_PROJECT/CSV_FILE/matches.csv";
        String line="";

        TreeMap<String,Integer> matchPerYear=new TreeMap<>();

        try {
            BufferedReader br =new BufferedReader(new FileReader(matches));
            while ((line= br.readLine())!=null)
            {
                String[] season = line.split(",");
                if(matchPerYear.containsKey(season[1]))
                {
                    matchPerYear.put(season[1],matchPerYear.get(season[1])+1);
                }
                else
                {
                    matchPerYear.put(season[1],1);
                }
            }

            System.out.println("Matches Played Per Year: ");

            for(Map.Entry<String,Integer> output : matchPerYear.entrySet())
            {
                System.out.println(output.getKey()+" = "+output.getValue());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}